# compile_library.mk

include $(F_PROJECT_ROOT)/pico-kit-foundation/build/definitions_common.mk
include $(F_PROJECT_ROOT)/pico-kit-foundation/build/definitions_library.mk
include $(F_PROJECT_ROOT)/pico-kit-foundation/build/variant/variant_loader.mk

# Check if C_SOURCES or CXX_SOURECES are empty
ifeq ($(strip $(LOC_C_SOURCES) $(LOC_CXX_SOURCES)),)
$(error No sources are specified to compile)
endif

# Check if MODULES is empty
ifeq ($(strip $(LOC_MODULES)),)
$(error No module are specified to build)
endif

missing_modules := $(filter-out $(LOC_MODULES),$(LOC_DEPENDENCIES))

ifneq ($(missing_modules),)
    $(error Missing modules: $(missing_modules))
endif

# Print project build information
ifeq ("$(V)","0")
$(info LIBRARY: $(F_PROJECT_NAME))
$(info MODULES: $(LOC_MODULES))
$(info SOURCES: $(words $(LOC_C_OBJECTS) $(LOC_CXX_OBJECTS)) C/C++ source file(s))
$(info DEFINES: $(LOC_DEFINES))
$(info INCLUDES: $(LOC_INC_DIRS))
$(info FEATURES: $(LOC_FEATURES))
$(info COMPILER: $(LOC_COMPILER_PATH)::$(LOC_COMPILER_TRIPLET)::$(LOC_COMPILER))
endif

# Force Make to not delete intermediate object files
.SECONDARY: $(LOC_C_OBJECTS) $(LOC_CXX_OBJECTS)

.DEFAULT_GOAL := all

.PHONY: all
all: $(F_PROJECT_LIB)

.PHONY: clean
clean:
	$(PRINT) " [RM]: $(LOC_BUILD_DIR)"
	$(VERBOSE)$(RM) $(LOC_BUILD_DIR)

.PHONY: size
size: $(F_PROJECT_LIB)
	size $<
