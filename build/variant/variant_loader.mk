# variant_loader.mk

LOC_COMPILER ?= gcc
LOC_FEATURES ?= debug

include $(F_PROJECT_ROOT)/pico-kit-foundation/build/variant/compiler_$(LOC_COMPILER).mk
include $(patsubst %,$(F_PROJECT_ROOT)/pico-kit-foundation/build/variant/feature_$(LOC_COMPILER)_%.mk,$(LOC_FEATURES))