# Makefile


# Configuration file settings
F_KCONFIG_CONFIG := .config
F_KCONFIG_CONFIG_OLD := $(F_KCONFIG_CONFIG).old
F_PROJECT_KCONFIG := build/Kconfig
F_DEFCONFIG_DIR := configurations
F_DEFCONFIG ?= $(F_DEFCONFIG_DIR)/full.defconfig

include build/definitions_common.mk

.PHONY: all
all: $(F_KCONFIG_CONFIG)
	$(VERBOSE)$(MAKE) -f Library.mk F_KCONFIG_CONFIG=$(F_KCONFIG_CONFIG)

.PHONY: clean
clean: $(F_KCONFIG_CONFIG)
	$(VERBOSE)$(MAKE) -f Library.mk F_KCONFIG_CONFIG=$(F_KCONFIG_CONFIG) clean

.PHONY: size
size: $(F_KCONFIG_CONFIG)
	$(VERBOSE)$(MAKE) -f Library.mk F_KCONFIG_CONFIG=$(F_KCONFIG_CONFIG) size

.PHONY: distclean
distclean:
	$(PRINT) " [RM] $(F_KCONFIG_CONFIG) $(F_KCONFIG_CONFIG_OLD) $(LOC_BUILD_DIR)"
	$(VERBOSE)$(RM) $(F_KCONFIG_CONFIG) $(F_KCONFIG_CONFIG_OLD) $(LOC_BUILD_DIR)

.PHONY: configure
configure: $(F_KCONFIG_CONFIG)

# Rule to list available defconfigs
.PHONY: configlist
configlist:
	@echo "Available defconfig files:"
	@for file in $(wildcard $(F_DEFCONFIG_DIR)/*.defconfig); do echo " $${file##*/}"; done

.PHONY: menuconfig
menuconfig:
	$(VERBOSE)KCONFIG_CONFIG=$(F_KCONFIG_CONFIG) python3 build/Kconfiglib/menuconfig.py $(F_PROJECT_KCONFIG)

$(F_KCONFIG_CONFIG):
	$(VERBOSE)KCONFIG_CONFIG=$(F_KCONFIG_CONFIG) python3 build/Kconfiglib/defconfig.py --kconfig $(F_PROJECT_KCONFIG) $(F_DEFCONFIG)

