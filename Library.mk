# Makefile.library

F_PROJECT_ROOT := ..

include $(F_PROJECT_ROOT)/pico-kit-foundation/$(F_KCONFIG_CONFIG)
include $(wildcard $(F_PROJECT_ROOT)/pico-kit-*/manifest.mk)
include $(F_PROJECT_ROOT)/pico-kit-foundation/build/compile_library.mk
